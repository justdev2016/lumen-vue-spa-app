// require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';

import routes from "./routes";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes
});
const App = require('./views/App.vue');

const app = new Vue({
    el: '#app',
    components: { App },
    router
});
