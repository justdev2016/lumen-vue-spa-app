const Home = () => import('./views/Home.vue')
const Hello = () => import('./views/Hello.vue')

export default [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/hello',
        name: 'hello',
        component: Hello
    },
    {
        path: '*',
        redirect: { name: 'home' }
    }
]
